#Game of three
Built using Nodejs+ SocketIO+Mocha+Shouldjs

#To run the server
node index.js

#To run the client
 - Open a window or tab on the browser with the link:
 - http://localhost:3250/
 - Do the same thing to have another player
 - if you open a third window/tab or more, the sessions are gonna be watchers and not players.

#Tests
- Used Mocha+Shouldjs
- run *npm test* or *mocha* to see the tests passing 