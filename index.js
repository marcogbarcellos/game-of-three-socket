var handler = function(req, res) {
	fs.readFile('./page.html', function (err, data) {
	    if(err) throw err;
	    res.writeHead(200);
		res.end(data);
	});
}
var app = require('http').createServer(handler);
var io = require('socket.io').listen(app);
var fs = require('fs');
var Moniker = require('moniker');
var port = 3250;

app.listen(port);

// game logic
var users = [];
var number = parseInt(Math.random()*100);

var addUser = function(id) {
	var player = true;
	var turn = false;
	if(users.length == 1){
		users[0].turn = true;
	}
  	else if(users.length>1){
  		player = false;
  	}
	var user = {
		name: Moniker.choose(),
		player:player,
		turn: turn,
		id: id
	}
	users.push(user);
	updateUsers();
	return user;
}
var removeUser = function(user) {
	for(var i=0; i<users.length; i++) {
		if(user.name === users[i].name) {
			users.splice(i, 1);
			updateUsers();
			return;
		}
	}
}
var updateUsers = function() {
	var str = '';
	for(var i=0; i<users.length; i++) {
		var user = users[i];
		str += user.name + '<br />';
		if(user.turn && io.sockets.connected[user.id]) {
		    io.sockets.connected[user.id].emit("turnOn", 
	    										{ user: user });			
		}else{
			io.sockets.connected[user.id].emit("turnOff", 
												{ user: user });			
		}
	}
	io.sockets.emit("users", 
				{ users: str, usersJson: JSON.stringify(users) });

}

var updateNumber = function(n,user) {
	if(number === 1){
		var msg = "We already have a winner.";
		io.sockets.emit("number", 
						{ number: number, user:user, 
						  callbackMessage: msg });
	}else{

		if(n === "-1"){
			number = (number - 1); 
		}else if(n === "1"){
			number = (number + 1); 
		}

		if(number%3 == 0){
			number = number/3; 	
		}

		if(number === 1){
			io.sockets.emit("winner", 
							{ winner: user.name, number: number });	
		}else{
			for(var i=0; i<users.length; i++) {
				var user = users[i];
				if(user.player) {
				    var tempUser = user;
					if(user.turn && io.sockets.connected[user.id]) {
					    tempUser.turn = false;
					    users[i] = tempUser;
					    io.sockets.connected[user.id].emit("turnOff", { user: users[i] });			
					}else{
						tempUser.turn = true;
					    users[i] = tempUser;
						io.sockets.connected[user.id].emit("turnOn", { user: users[i] });			
					}
				}	
			}
			io.sockets.emit("number", { number: number });	
		}
	}
}

// socket.io
io.sockets.on('connection', function (socket) {
	var user = addUser(socket.id);
	socket.emit("welcome", {user:user, number:number});
	
	socket.on('disconnect', function () {
		removeUser(user);
  	});
  	socket.on('number', function (data) {
		if(data.number){
			updateNumber(data.number,user);
		}
  	});

});