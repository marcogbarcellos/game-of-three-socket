var should = require('should');
var io = require('socket.io-client'),
    server = require('../index');



var socketURL = 'http://localhost:3250';
var options ={
  transports: ['websocket'],
  'force new connection': true
};

describe("Game Server",function(){

  it('1 user connected',function(done){
    var socket = io.connect(socketURL,options);
    socket.on('welcome', function (data) {
      data.should.have.property('user');
      socket.disconnect();
      done();
    });
  });
  it('2 User players, third user watcher',function(done){
    var socket1 = io.connect(socketURL,options);
    socket1.on('welcome', function (data) {
      data.user.should.have.property('player');
      data.user.player.should.be.equal(true);
      var socket2 = io.connect(socketURL,options);
      socket2.on('welcome', function (data2) {
        data2.user.should.have.property('player');
        data2.user.player.should.be.equal(true);
        var socket3 = io.connect(socketURL,options);
        socket3.on('welcome', function (data3) {
          data3.user.should.have.property('player');
          data3.user.player.should.not.be.equal(true);
          socket1.disconnect();
          socket2.disconnect();
          socket3.disconnect();
          done(); 
        });
      });
    });
  });
  it('the first user needs to wait another user to start its turn ',function(done){
    var socket1 = io.connect(socketURL,options);
    var user2;
    socket1.on('welcome', function (data) {
      data.user.turn.should.be.equal(false);
      var socket2 = io.connect(socketURL,options);
      socket2.on('turnOff', function (data) {
        socket2.disconnect();
      });
    });
    socket1.on('turnOn', function (data) {
      data.user.turn.should.be.equal(true);
      socket1.disconnect();
      done();
    });
  });
  it('If number is dividable by 3, then really have to be divided ',function(done){
    var socket1 = io.connect(socketURL,options);
    var number = 0;
    socket1.on('welcome', function (data) {
      number = parseInt(data.number);
      var socket2 = io.connect(socketURL,options);
      socket2.on('number', function (data) {
        data.number.should.be.equal(Math.round(number/3));
        socket1.disconnect();
        socket2.disconnect();
        done();
      });
      runDivision(number,socket1);
    });
  });

  it('check if number is 1 when game is over ',function(done){
    var socket1 = io.connect(socketURL,options);
    var number = 0;
    var turnPlayer1 = true;
    socket1.on('welcome', function (data) {
      number = parseInt(data.number);
      var socket2 = io.connect(socketURL,options);
      socket2.on('number', function (data) {
        number = parseInt(data.number);
        if(number>1){
          if(turnPlayer1){
           runDivision(number,socket1);
           turnPlayer1 = false; 
          }else{
            runDivision(number,socket2);
            turnPlayer1 = true; 
          }
        }
      });
      socket2.on('winner', function (data) {
        data.number.should.be.equal(1);
        socket1.disconnect();
        socket2.disconnect();
        done();
      });
      runDivision(number,socket1);
    });
  });
});

function runDivision(number,socket){
  if(number%3 == 0){
    socket.emit("number",{number:"0"});
  }else if(number%3 == 1 ){
    socket.emit("number",{number:"-1"});
  }else{
    socket.emit("number",{number:"1"});
  }
}